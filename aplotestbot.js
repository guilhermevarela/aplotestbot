var restify = require('restify');
var builder = require('botbuilder');
var http = require('http');
require('dotenv').config()

////ENABLE HTTPS
var local = true;

// var https_options = {};
// if (!local) {
//     var fs = require('fs');
//     https_options = {
//         key: fs.readFileSync('/etc/letsencrypt/live/demoz.online/privkey.pem'),
//         certificate: fs.readFileSync('/etc/letsencrypt/live/demoz.online/fullchain.pem'),
//     };
// }

// Next, we’ll ask the Restify server to listen for incoming messages on port 3978 
// (this is the port used by the bot framework to communicate with our bot).

//ENABLE HTTPS
// var server = restify.createServer(https_options);
var server = restify.createServer();

server.listen(process.env.port || process.env.PORT || 3978, function () {
    console.log('listening to %s', server.url);
});

// Create chat connector for communicating with the Bot Framework Service
var connector = new builder.ChatConnector({
    appId: process.env.MICROSOFT_APP_ID,
    appPassword: process.env.MICROSOFT_APP_PASSWORD
});
var bot = new builder.UniversalBot(connector);

//Now, add connector.listen() to catch messages on the /api/messages route.
server.post('/api/messages', connector.listen());
//With the syntax above, the bot.dialog function will 
//capture all incoming messages, and the session parameter will 
//contain all data relevant to the current conversation.

// Here, we are controlling the conversation flow by storing and checking values in the userData session variable, 
// which persists throughout the conversation.
bot.dialog('/', function (session, args) {
	 // First, we check whether session.userData.greeting doesn’t exist, 
	 //to show the user a greeting message. 
	 //To send this message to the user, session.send(“your message here”) is used. 
	 //After sending this message, we set the greeting session variable to true so that upon a response, it goes in the next IF condition.
    if (!session.userData.greeting) {
        session.send("Hello. What is your name?");
        session.userData.greeting = true;
    } else if (!session.userData.name) {
        getName(session);
    } else if (!session.userData.email) {
        getEmail(session);
    } else if (!session.userData.password) {
        getPassword(session);        
    } else {
        session.userData = null;
    }
    session.endDialog();
});

function getName(session) {
    name = session.message.text;
    session.userData.name = name;
    session.send("Hello, " + name + ". What is your Email ID?");
}

function getEmail(session) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    email = session.message.text;
    if (re.test(email)) {
        session.userData.email = email;
        session.send("Thank you, " + session.userData.name + ". Please set a new password.");
    } else {
        session.send("Please type a valid email address. For example: test@hotmail.com");
    }
}

// After we receive a valid email address, the user sets a new password, which is also validated against a regular expression. 
//If the test is passed, the new password is also saved in the session, and the sendData() function is called with the data that we saved in our session.userData
function getPassword(session) {
    var re = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/;
    password = session.message.text;
    if (re.test(password)) {
    	if (local) {
        console.log('getPassword local')
        var msg = "Thank you " + session.userData.name + " you're signed in. Please check your inbox for further details"
        session.send(msg);        
    	} else {
        console.log('getPassword linux')
        	session.userData.password = password;
        	var data = session.userData;
        	sendData(data, function (msg) {
            	session.send(msg);
            	session.userData = null;
        	});
      }
    } else {
        session.send("Password must contain at least 8 characters, including at least 1 number, 1 uppercase letter, 1 lowercase letter and 1 special character. For example: Mybot@123");
    }
}



// The sendData() function uses a http.get request to send the data (the first parameter being data) to an API and return the response in a callback (the second parameter being cb).
function sendData(data, cb) {
    http.get("http://local.dev/aplostestbot/saveData.php?name=" + data.name + "&email=" + data.email + "&password=" + data.password, function (res) {
        var msg = '';
        res.on("data", function (chunk) {
            msg += chunk;
        });

        res.on('end', function () {
            cb(msg);
        });

    }).on('error', function (e) {
        console.log("Got error: " + e.message);
    });
}